# PHP Google chart generator

PHP generates javascript that is google charts.

Example usage:

```php
$gchart = new gchart();
$gchart->elementById = "chart_out";
$gchart->title = 'Title';
$gchart->setColumns(['Date' => 'date', 'Number' => 'number']);

$gchart->setData([
['2019, 01',2], 
['2019, 02',4]
]);

echo $gchart->render();
```

### Example:

Run with docker
```
docker build -t example_gchart -f 'example/Dockerfile' .
docker run -d -p 80:80 example_gchart
```
Then open browser: http://localhost/example/

![google charts screenshot](example.png)
