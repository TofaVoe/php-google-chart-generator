<?php
/**
 * Class gchart
 * @author Kryštof Košut
 * @desc Class generating javascript code for google charts
 * at page this code is generated, you need to define its javascript content
 *
 * fe:
 * header('Content-Type: application/javascript');
 *
 * and you need to load libs to be able generating google charts.
 *
 *
 * In <head>:
 *
 * <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js"></script>
 * <script type = "text/javascript">
 *   google.charts.load('current', {'packages':['corechart', 'bar']});
 * </script>';
 *
 *
 */

class gchart
{
    /**
     * @var string
     * @deprecated
     */
    private $type = 'bar';
    /**
     * @var string
     */
    public $title = '';
    /**
     * @var string
     */
    public $subtitle;
    /**
     * @var array
     */
    private $columns = array();
    /**
     * @var array
     */
    private $data = array();

    /**
     * @var string
     */
    public $elementById;

    /**
     * @var array
     */
    private $options = array();

    /**
     * @var integer
     */
    public $height = 0;

    /**
     * @var integer
     */
    public $width = 0;

    /**
     * @var array
     * @desc array with RGB color codes
     * @example ['#CFB900', '#E14900']
     */
    public $colors = array();

    /**
     * @var string
     */
    public $topLabel = '';

    /**
     * @var string
     */
    public $leftLabel = '';

    /**
     * @var string
     */
    public $rightLabel = '';

    /**
     * @var string
     * @example 'en'
     */
    public $language = 'cs';

    /**
     * @var string
     * @example 'in'
        in - Draw the title inside the chart area.
        out - Draw the title outside the chart area.
        none - Omit the title.
     *
     */
    public $titlePosition = 'out';

    /**
     * @var string
     * @example 'in'
        in - Draw the axis titles inside the chart area.
        out - Draw the axis titles outside the chart area.
        none - Omit the axis titles.
     */
    public $axisTitlesPosition = 'out';

    /**
     * @var string
     * horizontal
     * vertical
     */
    public $orientation = 'horizontal';

    /**
     * @var string
     * @example 'red' or '#00cc00'
     */
    public $backgroundColor = 'white';

    /**
     * @var int
     */
    public $fontSize = 0;

    /**
     * @var string
     */
    public $fontName = 'Arial';

    /**
     * @var bool
     */
    public $forceIFrame = false;

    /**
     * @var bool
     * @desc The default is to draw left-to-right.
     */
    public $reverseCategories = false;

    /**
     * @var array
     * @example left:20,top:0,width:'50%',height:'75%'
     * @deprecated
     * @desc An object with members to configure the placement and size of the chart area (where the chart itself is drawn,
     * excluding axis and legends).
     * Two formats are supported: a number, or a number followed by %.
     * A simple number is a value in pixels; a number followed by % is a percentage
     */
    private $chartArea = array();

    /**
     * @var array
     * @desc Supported data types
     * @todo verify supported types? .. Google chart can identify types automatically, skipping for now.
     */
    private $supportedTypes = array('string', 'number', 'date', 'datetime', 'timeofday', 'boolean');

    /**
     * @var string
     */
    public $alignment = '';

    /**
     * @var string
     * @example 'maximized'
     * @desc only maximized is available at the moment.
     */
    public $theme = '';

    private $hasType = true;

    /**
     * @var array
     * @desc clip of types to render in JS, see renderData()
     */
    private $types = array();


    public function __construct()
    {
        // google chart type settings ("bar" only available), see $this->type
    }

    /**
     * @param array $data
     * @desc array containing arrays as data: @example [ [1,2,3] [2,3,4] [2,3,5] ] || [ [1,2], [3,2], ... ]
     * @note data with type 'date' or 'datetime' needs to be in string format 'YYYY, MM, DD'
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @todo
     * @deprecated
     */
    public function setChartArea()
    {
        // code here...
    }

    /**
     * @param array $columns
     * 'ColumnName' => 'columnType' or number to identify columnType automatically
     * Types:
     *  - string
     *  - number
     *  - boolean
     *  - date
     *  - datetime
     *  - timeofday
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param array $option
     * TODO: delete or finish?
     * @deprecated
     */
    public function addOption(array $option)
    {
        $this->options[] = $option;
    }

    private function renderHeader()
    {
        $output = PHP_EOL;

        $output .= "	
                google.charts.load('current', {packages:['corechart', 'bar'], 'language': '" . $this->language . "'});
	            google.charts.setOnLoadCallback(function(){".PHP_EOL;

        $output .= "var data = google.visualization.arrayToDataTable([".PHP_EOL;
        $output .= $this->renderColumns().PHP_EOL; // as set of objects {label: 'Sales', id: 'Sales', type: 'number'}, {label: 'Sales', id: 'Sales', type: 'number'}
        $output .= $this->renderData(); // as set of arrays [data1, data1, data1],[data2, data2, data2],[....]
        $output .= "]";
        if (!$this->hasType) $output .= ", false";
        $output .= ");".PHP_EOL;

        return $output;
    }

    /**
     * @return string
     */
    private function renderColumns()
    {
        // TODO: Handle errors
        // if(count($this->columns) == 0) // $this->error();
        $output = "[ ";
        $hasType = true;
        foreach ($this->columns as $label => $type) {
            if (is_int($type)) { // type not specified
                $output .= "'{$label}', ";
                $hasType = false;
            } else {
                $output .= "{ label: '{$label}', type: '{$type}' },".PHP_EOL;
                $this->types[] = $type;
            }
        }
        $this->hasType = $hasType;
        $output .= " ],";
        return $output;
    }

    /**
     * @return string
     */
    private function renderData()
    {
        $output = PHP_EOL;

        if (!$this->hasType) {
            foreach ($this->data as $index => $data) {
                $output .= "[";
                foreach ($data as $i => $datum) {
                    if (is_float($datum) || is_int($datum)) $output .= "{$datum}";
                    if (is_string($datum)) $output .= "'{$datum}'";
                    if (is_bool($datum)){
                        if ($datum){
                            $output .= "true";
                        } else {
                            $output .= "false";
                        }
                    }
                    $output .= ", ";
                }
                $output .= "],".PHP_EOL;
            }
        } else {
            foreach ($this->data as $index => $data) {
                $output .= "[";
                foreach ($data as $i => $datum) {
                    $output .= " ";
                        switch ($this->types[$i]){
                            case 'datetime':
                            case 'date':
                                $output .= "new Date('{$datum}')";
                                break;

                            case 'boolean':
                                if (is_bool($datum)){
                                    if ($datum){
                                        $output .= "true";
                                    } else {
                                        $output .= "false";
                                    }
                                }
                                break;

                            case 'timeofday':
                                // todo: finish
                                break;

                            default:
                                $output .= "'{$datum}'";
                                break;
                        }
                    $output .= ", ";
                }
                $output .= "],".PHP_EOL;
            }
        }

        return $output;
    }

    /**
     * @return string
     */
    private function getOptionData()
    {
        $output = PHP_EOL;

        if ($this->title !== '' || $this->subtitle) $output .= "chart: { title: '{$this->title}', subtitle: '{$this->subtitle}' },".PHP_EOL;
        if ($this->width !== 0) $output .= "width: {$this->width},".PHP_EOL;
        if ($this->height !== 0) $output .= "height: {$this->height},".PHP_EOL;
        if ($this->fontSize !== 0) $output .= "fontSize: {$this->fontSize},".PHP_EOL;
        if ($this->forceIFrame !== false) $output .= "forceIFrame: {$this->forceIFrame},".PHP_EOL;
        if ($this->reverseCategories !== false) $output .= "reverseCategories: {$this->reverseCategories},".PHP_EOL;
        if ($this->theme !== '') $output .= "theme: '{$this->theme}',".PHP_EOL;

        $output .= "fontName: '{$this->fontName}',".PHP_EOL;
        $output .= "axisTitlesPosition: '" . $this->axisTitlesPosition . "',".PHP_EOL;
        $output .= "backgroundColor: '" . $this->backgroundColor . "',".PHP_EOL;
        $output .= "titlePosition: '" . $this->titlePosition . "',".PHP_EOL;

        // $output .= "animation: {" . $this->animationData(). "}, ".PHP_EOL;
        // $output .= "legend: {" . $this->legendData() . "}, ".PHP_EOL;

        if (count($this->colors) != 0) {
            $output .= "colors: [";
            foreach ($this->colors as $color) {
                $output .= "'{$color}', ";
            }
            $output .= "],".PHP_EOL;
        }
        $output .= "axes: {" . $this->getAxesData() . "},".PHP_EOL;

        return $output;
    }

    /**
     * @return string
     * @deprecated
     */
    private function getLegendData()
    {
        $output = "";

        /**
        'bottom' - Below the chart.
        'left' - To the left of the chart, provided the left axis has no series associated with it. So if you want the legend on the left, use the option targetAxisIndex: 1.
        'in' - Inside the chart, by the top left corner.
        'none' - No legend is displayed.
        'right' - To the right of the chart. Incompatible with the vAxes option.
        'top' - Above the chart.
         */
        $output .= "position: 'right', "; // Nevidim zmenu

        /**
        'start' - Aligned to the start of the area allocated for the legend.
        'center' - Centered in the area allocated for the legend.
        'end' - Aligned to the end of the area allocated for the legend.
         */
        if ($this->alignment !== '') $output .= "alignment: {$this->alignment},";

        /**
         * @todo
         */
        // $output .= "textStyle: ";    <

        return $output;
    }

    /**
     * @return string
     */
    private function getAxesData()
    {
        $output = '';

        if ($this->topLabel !== '') $output .= " x: { 0: { side: 'top', label: '{$this->topLabel}'} } ";
        if ($this->leftLabel !== '') $output .= " y: { 0: { side: 'left', label: '{$this->leftLabel}'} } ";
        if ($this->rightLabel !== '') $output .= " v: { 0: { side: 'right', label: '{$this->rightLabel}'} } ";


        return $output;
    }

    /**
     * @return string
     * @deprecated
     * todo: Finish or delete?
     * https://developers.google.com/chart/interactive/docs/animation
     */
    private function getAnimationData()
    {
        $output = "";
        $output .= " startup: 'true', ";
        $output .= " duration: 10000,";
        $output .= " easing: 'in',";

        return $output;
    }

    /**
     * @return string
     * https://developers.google.com/chart/interactive/docs/roles
     */
    private function getAnnotationData()
    {
        $output = "";

        return $output;
    }

    /**
     * @return string
     * @deprecated
     * todo: finish or delete
     */
    private function getHAxisData()
    {
        $output = '';
        $output .= "title: 'H AXIS', textPosition: 'out', baselineColor: 'red'   ";

        return $output;
    }

    /**
     * @return string
     * @deprecated
     * todo: finish or delete
     */
    private function getVAxisData()
    {
        $output = '';
        $output .= "title: 'V AXIS', textPosition: 'out', baselineColor: 'red'   ";

        return $output;
    }

    /**
     * @return string
     */
    private function renderFooter()
    {
        $output = PHP_EOL;

        $output .= "
        let options = { " . $this->getOptionData() . " };

		let materialChart = new google.charts.Bar(document.getElementById('{$this->elementById}'));
		materialChart.draw(data, options);
	});";

        return $output;
    }

    /**
     * @return string javascript
     */
    public function render()
    {
        return $this->renderHeader() . $this->renderFooter();
    }
}
