<html>
<head>
    <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js"></script>
    <script type = "text/javascript">
        google.charts.load('current', {'packages':['corechart', 'bar']});
    </script>
</head>
<body>
<h2>Example:</h2>
<div id="chart_out"></div>

<script type = "text/javascript">
<?php
require_once '../gchart.php';


$gchart = new gchart();
$gchart->elementById = "chart_out";
$gchart->title = 'Title';
$gchart->setColumns(['Date' => 'date', 'Number' => 'number']);

$gchart->setData([
    ['2019, 01',2],
    ['2019, 02',4]
]);

echo $gchart->render();

?>
</script>
</body>
